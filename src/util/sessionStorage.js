export default {
    get (key) {
        return JSON.parse(window.sessionStorage.getItem(key) || '')
    },
    
    save (key, item) {
        window.sessionStorage.setItem(key, JSON.stringify(item))
    }
}