export default {
    get (key) {
        return JSON.parse(window.localStorage.getItem(key) || '')
    },

    save (key, item) {
        window.localStorage.setItem(key, JSON.stringify(item))
    },

    clear (array) {
        for (let item of array) {
            window.localStorage.removeItem(item)
        }
    }
}