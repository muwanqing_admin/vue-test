
 /**
 * 异步加载组件
 *
 * @param {string} componentName 组件地址
 * @returns
 */
export const asyncUploadComponet = (componentName) => {
    return () => import ('@view/' + componentName)
}