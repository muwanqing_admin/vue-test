import axios from 'axios'

class HttpService {
    constructor(params) {
        const { item } = params

        if (axios.interceptors.request.handlers.length === 0) {
            let baseParams = { item }

            let baseUrl = '/baseurl'

            this.configAxios(baseParams, baseUrl)
        }
    }

    configAxios(baseParams, baseUrl) {
        axios.defaults.baseUrl = baseUrl
        axios.defaults.timeout = 10000

        axiosn.interceptors.request.use(
            request => {
                if (request.method === 'post') {
                    console.log('数据进行处理' + baseParams)
                } else if (request.method === 'get') {
                    console.log('数据转为get请求类型' + baseParams)
                }
                return request
            },

            error => {
                return Promise.reject(error)
            }
        )

        axios.interceptors.response.use(
            response => {
                let body = response.data

                if (body.status !== '200') {
                    console.log('提示出错')
                }

                return body
            },

            error => {
                return Promise.reject(error)
            }
        )
    }
}

export default HttpService