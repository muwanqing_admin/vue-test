import Vue from 'vue'
import Router from 'vue-router'
// import HelloWorld from '@/components/HelloWorld'
// import echarts from '@/components/echarts'
// import table from '@/components/table'
// import one from '@/components/one'
// import two from '@/components/two'
// import three from '@/components/three'
// import select from '@/components/select'
// import ElementUpload from '@/view/upload'

function uploadCom(com) {
  return  r => require.ensure([], () => r(require('@/components/'+com+'.vue')))
}


import one from './routes/one'
Vue.use(Router)

const router = new Router({
  routes: [{
      path: '/',
      name: 'HelloWorld',
      component: uploadCom('HelloWorld'),
    },
    {
      path: '/upload',
      component: () =>
        import ('@/view/upload')
    },
    {
      path: '/select',
      name: 'select',
      component: uploadCom('select')
    },
    {
      path: '/one',
      name: 'one',
      component: uploadCom('one'),
      children: [{
        name: 'two',
        path: 'two',
        component: uploadCom('two'),
        children: [{
          name: 'three',

          path: 'three',
          component: uploadCom('three')
        }]
      }]
    },
    {
      path: '/echarts',
      name: 'echarts',
      component: uploadCom('echarts'),

    },
    {
      path: '/table',
      name: 'table',
      component: uploadCom('table')
    },

    {
      path: '/nav_menu',
      name: 'NavMenu',
      component: () =>
        import ('./../components/NavMenu.vue'),
      children: [{
          path: 'nav_menu_one',
          meta: {
            flag: 'one'
          },
          component: () =>
            import ('./../components/NavMenuOne.vue')
        },
        {
          path: 'nav_menu_two',
          meta: {
            flag: 'two'
          },
          component: () =>
            import ('./../components/NavMenuTwo.vue')
        },
        {
          path: 'nav_menu_three',
          meta: {
            flag: 'three'
          },
          component: () =>
            import ('./../components/NavMenuThree.vue')
        },
      ]
    },

    {
      path: '/error',
      name: 'Err',
      component: () =>
        import ('./../components/error.vue')
    },
    ...one

  ]
})

// router.beforeEach((to, from, next) => {
//   const USER_MENU = ['one', 'three']
//   if (USER_MENU.includes(to.meta.flag)) {
//     next()
//   } else {
//     if (to.path === '/error') {
//       next()
//     } else {
//       next('/error')
//     }
//   }

// })

export default router


/**
 * 将路由分模块处理
 */

//  import Vue from 'vue'
//  import Router from 'vue-router'

//  Vue.use(Router)

//  import one from './routes/one'

//  let routes = [].concat(
//    one
//  )

//  export default new Router({
//    routes,
//  })