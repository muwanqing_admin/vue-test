let baseUrl = ''

if (process.env.NODE_ENV === 'development') {
    baseUrl = 'https://173.6.11:8000'
} else {
    baseUrl = 'http://' + window.location.href
}


const one = {
        type: 'get',
        url: '/one.api'
    }


export default {
    baseUrl,
    one,
}