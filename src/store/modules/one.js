import Service from './../../service/services/one'

export default {
    namespaced: true, // 添加这个之后只是为了更高的复用性
    actions: {
        getList ({}, payLoad) { // 必须占位
            let service = new Service()
            
            return service.getList(payLoad)
        } 
    }
}