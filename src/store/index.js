import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import state from './state'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

import one from './modules/one'

export default new Vuex.Store({
    state,
    mutations,
    getters,
    actions,
    modules: {
        one,
    }
})