# vue-p

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## 测试 `upload`中 
> 由于需要在文件上传前进行判断，因而需要处理
+ 手动上传  `auto-upload=false ` 结合 `on-change`
```
<el-upload 
    ref="upload"
    class="upload-demo" 
    action="https://jsonplaceholder.typicode.com/posts/" 
    :on-preview="handlePreview" 
    :limit="1" 
    :auto-upload="false"
    :on-change="what"
    :file-list="fileList">
      <el-button size="small" type="primary" @click="checkImg">点击上传</el-button>
      <div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且不超过500kb</div>
    </el-upload>

 async what() {
      if (this.num > 1) return
      console.log(1)
      const bo = await this.$confirm('此操作将永久删除该文件, 是否继续?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          return false
        }).catch(() => { 
          return true        
        });
        console.log(bo)
        if (bo) {
          this.$refs.upload.submit()
          
        this.num ++ 
        } else {
          this.fileList = []
        }
    }

```
+ `before-upload`中使用
